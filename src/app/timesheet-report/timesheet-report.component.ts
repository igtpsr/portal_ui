import { AdminService } from './../services/admin.service';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';
import { TimesheetService } from './../services/timesheet.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-timesheet-report',
  templateUrl: './timesheet-report.component.html',
  styleUrls: ['./timesheet-report.component.css']
})
export class TimesheetReportComponent implements OnInit {

  clientName: string = "";
  showStatus: boolean = true;
  clientNameList: any = [];
  startDate: any = "";
  endDate: any = "";
  @ViewChild('myGrid') myGrid: jqxGridComponent;

  constructor(private timesheetService: TimesheetService, private adminService: AdminService) { }

  ngOnInit(): void {
    this.adminService.getClientNames().subscribe((response) => {
      let res = response;
      this.clientNameList = res;
    },
      error => {
        console.error(error)
      })
  }

  data = new Array();



  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '75%';
    }

    return '40%';
  }
  columns: any = [];
  onGetTimesheetReport(): void {
    this.myGrid.showloadelement();
    if (this.clientName != "" && this.startDate != "" && this.endDate != "") {
      this.timesheetService.getTimesheetReport(this.clientName, this.startDate, this.endDate).subscribe(
        response => {
          console.log(response);
          let res: any[] = response;
          this.data = [];
          this.myGrid.hideloadelement();
          if (res.length > 0) {
            this.showStatus = false;

            for (let i = 0; i < res.length; i++) {
              this.data.push({ empId: res[i].empId, clientName: res[i].clientName, projectId: res[i].projectId, totalHours: res[i].totalHours })
            }
            this.source.localdata = this.data;

            console.log(this.data);
            this.columns =
              [{ text: 'Employee Id', columntype: 'string', datafield: 'empId', width: 150 },
              { text: 'Client Name', columntype: 'string', datafield: 'clientName', width: 150 },
              { text: 'Project Id', datafield: 'projectId', columntype: 'string', width: 100 },
              {
                text: 'Total Hours', datafield: 'totalHours', columntype: 'int', width: 100, aggregates: ['sum'], aggregatesrenderer: (aggregates: any): string => {
                  // console.log("coming", aggregates)
                  let renderstring = '';
                  for (let obj in aggregates) {
                    let name = 'Total'
                    let value = aggregates[obj];
                    console.log("value--" + value)
                    renderstring += '<div style="position: relative; margin: 4px; overflow: hidden;">' + name + ': ' + value + '</div>';
                  }
                  return renderstring;
                }
              },
              ];
          }
          else {
            this.myGrid.clear();
            alert("No data to show");
          }
        },
        error => {
          console.log(error);
          console.log("Error in getTimesheetReport");
        },
      );
    }
    else {
      alert("All fields are mandatory.");
    }


  }
  source: any =
    {
      localdata: this.data,
      datatype: 'array',
      datafields:
        [
          { name: 'clientName', type: 'string' },
          { name: 'empId', type: 'string' },
          { name: 'projectId', type: 'string' },
          { name: 'totalHours', type: 'int' },
        ]
    };

  dataAdapter: any = new jqx.dataAdapter(this.source);

}
