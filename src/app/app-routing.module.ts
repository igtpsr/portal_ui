import { TimesheetReportComponent } from './timesheet-report/timesheet-report.component';
import { NewTimesheetComponent } from './new-timesheet/new-timesheet.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { ErrorComponent } from './error/error.component';
import { AppComponent } from './app.component';
import { SiteLayoutComponent } from './_layout/site-layout/site-layout.component';
import { DetailsComponent } from './details/details.component'
import { AuthGuard } from './guards/auth-guard.service';


const routes: Routes = [
  {
    path: '',
    component: SiteLayoutComponent,
    children: [
      { path: 'configure', component: ConfigurationComponent },
      { path: 'details', component: DetailsComponent },

    ]
  },
  { path: 'login', component: LoginComponent },
  {
    path: '', component: SiteLayoutComponent,
    children: [
      { path: 'new-timesheet', component: NewTimesheetComponent },
    ]
  },
  {
    path: '', component: SiteLayoutComponent,
    children: [
      { path: 'timesheet', component: TimesheetComponent },
    ]
  },
  {
    path: '', component: SiteLayoutComponent,
    children: [
      { path: 'timesheet-report', component: TimesheetReportComponent },
    ]
  },
  { path: 'timesheet', component: TimesheetComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
