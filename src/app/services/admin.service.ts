import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import { Http, RequestOptions, Headers } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdminDetail } from '../classes/admin-detail';
import { Router } from '@angular/router';

//import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  // Base URL
  private baseUrl = "http://localhost:8080/api/";

  token = localStorage.getItem('token');
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this.token
  })

  constructor(private http: HttpClient, private router: Router) { }

  saveAdminDetails(adminDetail: AdminDetail): Observable<any> {
    let url = this.baseUrl + "saveAdmin";
    return this.http.post(url, adminDetail);
  }


  login(adminDetail: AdminDetail): Observable<any> {
    let url = this.baseUrl + "login";
    return this.http.post(url, adminDetail, { observe: 'response' as 'body' });
  }

  logout() {
    let url = this.baseUrl + "logout";
    // Remove the token from the localStorage.
    this.http.get(url).subscribe(response => {
      if (response == 1) {
        //alert('Successfully Logut');
        localStorage.removeItem('token');
        localStorage.removeItem('id');
        this.router.navigate(['/login']);
      }
    }, error => {
      alert('Error in logout');
      console.log(error);
    });

  }

  changeLog() {
    this.router.navigate(['/changelog']);
  }
  /*
	* Check whether User is loggedIn or not.
	*/

  isLoggedIn() {

    // create an instance of JwtHelper class.
    //let jwtHelper = new JwtHelperService();

    // get the token from the localStorage as we have to work on this token.
    let token = localStorage.getItem('token');

    // check whether if token have something or it is null.
    if (!token) {
      return false;
    }

    // get the Expiration date of the token by calling getTokenExpirationDate(String) method of JwtHelper class. this method accept a string value which is nothing but token.

    if (token) {
      // let expirationDate = jwtHelper.getTokenExpirationDate(token);

      // check whether the token is expired or not by calling isTokenExpired() method of JwtHelper class.

      //let isExpired = jwtHelper.isTokenExpired(token);

      //return !isExpired;
    }
  }


  getAdminDetail(adminId): Observable<any> {
    let url = this.baseUrl + "getAdminData/" + adminId;
    let headers = new HttpHeaders();
    let token = localStorage.getItem('token');
    headers.set('Authorization', 'Bearer ' + token);
    return this.http.get(url, { headers });
  }


  saveEnvironment(environmentData: any): Observable<any> {
    console.log(environmentData)
    let url = this.baseUrl + "saveEnvironment";
    let headers = new HttpHeaders();
    let token = localStorage.getItem('token');
    headers.set('Authorization', 'Bearer ' + token);
    return this.http.post(url, environmentData, { headers });
  }

  getEnvironment(): Observable<any> {

    let url = this.baseUrl + "getEnvironment";
    let headers = new HttpHeaders();
    let token = localStorage.getItem('token');
    headers.set('Authorization', 'Bearer ' + token);
    return this.http.get(url);
  }
  getClientNames(): Observable<any> {
    let url = this.baseUrl + "getClientNames";
    return this.http.get(url, {headers:this.headers});
  }

}