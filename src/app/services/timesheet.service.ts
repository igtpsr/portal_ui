import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class TimesheetService {

    // Base URL
    private baseUrl = "http://localhost:8080/api/";

    token = localStorage.getItem('token');
    headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.token
    })
    constructor(private http: HttpClient, private router: Router) { }

    saveTimesheet(timesheetData: any): Observable<any> {
        let url = this.baseUrl + "saveTimesheet";
        return this.http.post(url, timesheetData, { headers: this.headers });

    }

    getTimesheetDetails(empId: string, startDate: string, endDate: string): Observable<any> {
        let url = this.baseUrl + "getTimesheetByEmpID?empId=" + empId + "&fromDate=" + startDate + "&toDate=" + endDate;
        return this.http.get(url, { headers: this.headers });
    }

    getTimesheetReport(clientName: string, startDate: any, endDate: any): Observable<any> {
        let url = this.baseUrl + "getTimesheetReportByClientName?clientName=" + clientName + "&fromDate=" + startDate + "&toDate=" + endDate;
        return this.http.get(url, { headers: this.headers });
    }

}