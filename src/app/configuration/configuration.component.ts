import { Component, OnInit, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { Animations } from '../animation';
import { FormGroup, Validators, FormControl, FormArray, FormBuilder,NgForm  } from '@angular/forms';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  animations: [
    Animations.animeTrigger
  ]
  
})
export class ConfigurationComponent implements OnInit {
    
  environmentGroup:FormGroup;
  moduleGroup:FormGroup;
  
  envDatas:any = [];

  modDatas = [{
      modName:"Security",
      modDetails:['ssss']
  }];

  constructor(private elRef:ElementRef, private formBuilder: FormBuilder, private adminService: AdminService) {}
  
  
  ngOnInit() {

    this.adminService.getEnvironment().subscribe(response=>{
      let data:any = response;
      for(let i = 0; i<data.length; i++) {
        this.envDatas.push({
          envName:data[i].environmentName,
          envDetails:{
            dbUserId:data[i].userId,
            dbPwd:data[i].password,
            dbServerName:data[i].serverName,
            portNo:data[i].portNumber,
            dbName:data[i].databaseName,
            dataType:data[i].dataType,
            databaseType:data[i].databaseType
          }
        })
      }
    });
  }


addNewEnv() {

    let envValue = document.getElementById('addEnv') as HTMLInputElement;
    
    if(envValue.value.length<=0) {
        alert("Please Enter Environemnt Name");
        return;
    }
    this.envDatas.push({
      envName:envValue.value,
      envDetails: {
        dbUserId:'',
        dbPwd:'',
        dbServerName:'',
        portNo:'',
        dbName:'',
        dataType:'',
        databaseType:''
       }

      }
    )
}

addModule() {
    let modValue = document.getElementById('addModText') as HTMLInputElement;
    
    if(modValue.value.length<=0) {
      alert("Please Enter Module Name");
      return;
    }
    this.modDatas.push({
      modName:modValue.value,
      modDetails:[]
    });

  }

  onSubmit(index, formid) {
    let formElements = document.getElementById(formid) as HTMLFormElement;
    let formData = new FormData(formElements);
    let storeForm:any = [];
    storeForm.push({
      'environmentName':this.envDatas[index].envName,
      'userId':formData.get('userId'+index),
      'password':formData.get('password'+index),
      'serverName':formData.get('serverName'+index),
      'portNumber':formData.get('portNumber'+index),
      'databaseName':formData.get('databaseName'+index),
      'dataType':formData.get('dataType'+index),
      'databaseType':formData.get('databaseType'+index)
    });
    this.adminService.saveEnvironment(storeForm).subscribe(
      response=>{
        console.log(response);
      }
    )
  }

  addTableinList(index:string) {
    let modTableInputValue = document.getElementById('addtable'+index) as HTMLInputElement;
    
    if(modTableInputValue.value.length<=0) {
       alert("Please Enter Table Name");
      return;
    }
    this.modDatas[index].modDetails.push(modTableInputValue.value);
  }

  deleteTable(mainindex,tableIndex) {
    this.modDatas[mainindex].modDetails.splice(tableIndex,1);
  }

}
