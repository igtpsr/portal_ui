import { TimesheetService } from './../services/timesheet.service';
import { TimesheetDetails } from './../classes/timesheet-details';
import { TimesheetHeader } from './../classes/timesheet-header';
import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit } from '@angular/core';
import { jqxCalendarComponent } from 'jqwidgets-ng/jqxcalendar';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid'

@Component({
  selector: 'app-new-timesheet',
  templateUrl: './new-timesheet.component.html',
  styleUrls: ['./new-timesheet.component.css']
})
export class NewTimesheetComponent implements OnInit, AfterViewInit {

  @ViewChild('myGrid') myGrid: jqxGridComponent;
  @ViewChild('myCalendar', { static: false }) myCalendar: jqxCalendarComponent;
  @ViewChild('log', { static: false }) log: ElementRef;
  startingDate: any = null;
  endingDate: any = null;
  start: any;
  end: any;
  showStatus: boolean = true;

  private timesheetHeader = new TimesheetHeader();
  private timesheetDetails = new TimesheetDetails();

  constructor(private timesheetService: TimesheetService) { }

  ngOnInit(): void {
    console.log("coming");

  }
  ngAfterViewInit(): void {
  }


  temp = new Array();
  source: any =
    {
      localdata: this.temp,
      datatype: 'array',
      datafields:
        [
          { name: 'client', value: 'client' },
          { name: 'projectId', type: 'string' },
          { name: 'task', type: 'string' },
          { name: 'billableType', type: 'string' },
          { name: 'Total', type: 'number' },
        ]
    };
  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '75%';
    }

    return '95%';
  }
  totalHours = 0;

  // columns: any[] =
  //   [
  //     {
  //       text: 'Client Name', columntype: 'dropdownlist', datafield: 'client', width: 150, createeditor: function (row, cellvalue, editor) {
  //         let data = new Array();
  //         data.push({ client: 'MGF' });
  //         data.push({ client: 'AE' });
  //         editor.jqxDropDownList({ displayMember: 'client', valueMember: 'client', source: data });
  //       }
  //     },
  //     { text: 'Project Id', datafield: 'projectId', columntype: 'textbox', width: 100 },
  //     { text: 'Task', datafield: 'task', columntype: 'textbox', width: 500 },
  //     {
  //       text: 'Billable Type', columntype: 'dropdownlist', datafield: 'billableType', width: 150, createeditor: function (row, cellvalue, editor) {
  //         let data = new Array();
  //         data.push({ type: 'Billable' });
  //         data.push({ type: 'Non-Billable' });
  //         editor.jqxDropDownList({ displayMember: 'type', valueMember: 'type', source: data });
  //       }
  //     },
  //     // {
  //     //   text: 'Mon', datafield: 'Mon', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Tue', datafield: 'Tue', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Wed', datafield: 'Wed', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Thur', datafield: 'Thur', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Fri', datafield: 'Fri', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Sat', datafield: 'Sat', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },
  //     // {
  //     //   text: 'Sun', datafield: 'Sun', width: 70, align: 'center', cellsalign: 'center', columntype: 'numberinput'
  //     // },

  //     // {
  //     //   text: 'Total', editable: false, datafield: 'total', width: 105, align: 'center', columntype: 'numberinput', cellsalign: 'center',
  //     //   aggregates: [{
  //     //     '<b>Total</b>': (aggregatedValue: number, currentValue: number, column: any, rowdata: any): number => {
  //     //       let total = aggregatedValue + currentValue;
  //     //       if (!isNaN(total))
  //     //         return total;
  //     //       else
  //     //         return aggregatedValue;
  //     //     }
  //     //   }]
  //     // }
  //   ];
  getTotalValue(value): number {
    if (!isNaN(parseInt(value)))
      return 0 + parseInt(value);
    else
      return 0;
  }

  rendertoolbar = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let buttonContainer1 = document.createElement('div');
    let buttonContainer2 = document.createElement('div');
    buttonContainer1.id = 'buttonContainer1';
    buttonContainer2.id = 'buttonContainer2';
    buttonContainer1.style.cssText = 'float: left';
    buttonContainer2.style.cssText = 'float: left; margin-left: 5px';
    container.appendChild(buttonContainer1);
    container.appendChild(buttonContainer2);
    toolbar[0].appendChild(container);
    let addRowButton = jqwidgets.createInstance('#buttonContainer1', 'jqxButton', { width: 105, value: 'Add New Row' });
    let deleteRowButton = jqwidgets.createInstance('#buttonContainer2', 'jqxButton', { width: 150, value: 'Delete Selected Row' });
    addRowButton.addEventHandler('click', () => {
      let datarow = { client: '', projectId: '', task: '', billableType: '', Total: '' }
      for (var day = this.startingDate; day <= this.endingDate; day.setDate(day.getDate() + 1)) {
        console.log(day)
        var today = day.getDate();
        var month = day.getMonth() + 1;
        var year = day.getFullYear();
        var nDate = today + '-' + month + '-' + year;
        datarow['"' + nDate.toString() + '"'] = '';
      }
      this.myGrid.addrow(null, datarow);
    })
    deleteRowButton.addEventHandler('click', () => {
      let selectedrowindexes = this.myGrid.getselectedrowindexes();
      console.log(selectedrowindexes)
      let rowscount = this.myGrid.getdatainformation().rowscount;
      selectedrowindexes.forEach(selectedrowindex => {
        console.log(selectedrowindex)
        if (selectedrowindex >= 0) {
          let id = this.myGrid.getrowid(selectedrowindex);
          this.myGrid.deleterow(id);
        }
      });

    })
  };
  generateClientData(): any[] {
    let data = new Array();
    data.push({ client: 'MGF' });
    data.push({ client: 'AE' });
    return data;
  }


  startDate(event: any): void {
    let selection = event.args;
    this.startingDate = selection.date;
    var today1 = this.startingDate.getDate();
    var month1 = this.startingDate.getMonth() + 1;
    var year1 = this.startingDate.getFullYear();
    this.start = year1 + '/' + month1 + '/' + today1;
    console.log(this.startingDate);
  }

  endDate(event: any): void {
    let selection = event.args;
    this.endingDate = selection.date;
    var today2 = this.endingDate.getDate();
    var month2 = this.endingDate.getMonth() + 1;
    var year2 = this.endingDate.getFullYear();
    this.end = year2 + '/' + month2 + '/' + today2;
    console.log(this.endingDate);
  }
  columns: any[];
  onGenerate(): void {
    console.log(this.start);
    console.log(this.end);
    if (this.start > this.end) {
      alert("Always End date should be greater than start Date");
      return;
    }
    if (this.start != null && this.end != null) {
      this.showStatus = false;
      // let selection = event.args.range;
      // console.log(event.args)
      // console.log(new Date(selection.from))
      // console.log(selection.to)
      // this.startingDate = selection.from;
      // this.endingDate = selection.to;

      // var today1 = this.startingDate.getDate();
      // var month1 = this.startingDate.getMonth() + 1;
      // var year1 = this.startingDate.getFullYear();
      // this.start = year1 + '/' + month1 + '/' + today1;

      // var today2 = this.endingDate.getDate();
      // var month2 = this.endingDate.getMonth() + 1;
      // var year2 = this.endingDate.getFullYear();
      // this.end = year2 + '/' + month2 + '/' + today2;

      console.log(this.startingDate)
      console.log(this.endingDate)

      this.columns =
        [
          {
            text: 'Client Name', columntype: 'dropdownlist', datafield: 'client', width: 150, createeditor: function (row, cellvalue, editor) {
              let data = new Array();
              data.push({ client: 'MGF' });
              data.push({ client: 'AE' });
              editor.jqxDropDownList({ displayMember: 'client', valueMember: 'client', source: data });
            }
          },
          { text: 'Project Id', datafield: 'projectId', columntype: 'textbox', width: 100 },
          { text: 'Task', datafield: 'task', columntype: 'textbox', width: 410 },
          {
            text: 'Task Type', columntype: 'dropdownlist', datafield: 'billableType', width: 150, createeditor: function (row, cellvalue, editor) {
              let data = new Array();
              data.push({ type: 'Billable' });
              data.push({ type: 'Non-Billable' });
              editor.jqxDropDownList({ displayMember: 'type', valueMember: 'type', source: data });
            }
          }];

      let count = 1
      for (var day = new Date(this.start); day <= new Date(this.end); day.setDate(day.getDate() + 1)) {
        console.log(day)
        var today = day.getDate();
        var month = day.getMonth() + 1;
        var year = day.getFullYear();
        var nDate = today + '-' + month + '-' + year;
        this.source.datafields.push({ name: today, type: "number" });
        this.columns.push({
          text: "<div style=height:16px;>" + day.toString().split(' ')[0] + "</div>" + nDate.toString(), datafield: '"' + nDate.toString() + '"', width: 120, align: 'center', cellsalign: 'center', columntype: 'numberinput', aggregates: ['sum'],
          aggregatesrenderer: (aggregates: any): string => {
            // console.log("coming", aggregates)
            let renderstring = '';
            for (let obj in aggregates) {
              let name = 'Total'
              let value = aggregates[obj];
              console.log("value--" + value)
              renderstring += '<div style="position: relative; margin: 4px; overflow: hidden;">' + name + ': ' + value + '</div>';
            }
            return renderstring;
          }
        });
        console.log(this.columns)
        console.log(nDate)
        console.log(count)
        count = count + 1

      }

      this.columns.push(
        {
          text: 'Total', editable: false, datafield: 'total', width: 105, align: 'center', columntype: 'numberinput', cellsalign: 'center',
          aggregates: [{
            '<b>Total</b>': (aggregatedValue: number, currentValue: number, column: any, rowdata: any): number => {
              let total = aggregatedValue + currentValue;
              if (!isNaN(total)) {
                this.totalHours = total;
                return total;
              }
              else
                return aggregatedValue;
            }
          }]
        }
      );

      this.myGrid.beginupdate();
      this.myGrid.setOptions
        ({
          source: this.dataAdapter,
          columns: this.columns
        });
      this.myGrid.endupdate();

    }
    else {
      alert("Start date and End date are mandatory fields");
    }

  }
  dataAdapter: any = new jqx.dataAdapter(this.source);

  cellBeginEditEvent(event: any): void {
    console.log(event);
    let args = event.args;
    let rowdata = this.myGrid.getrowdata(args.rowindex);
    console.log("rowdata--->", rowdata);
    // let total = this.getTotalValue(rowdata.Mon) + this.getTotalValue(rowdata.Tue) + this.getTotalValue(rowdata.Wed)
    //   + this.getTotalValue(rowdata.Thur) + this.getTotalValue(rowdata.Fri) + this.getTotalValue(rowdata.Sat)
    //   + this.getTotalValue(rowdata.Sun);
    let total = 0;
    console.log(new Date(this.start))
    console.log(new Date(this.end))
    for (var day = new Date(this.start); day <= new Date(this.end); day.setDate(day.getDate() + 1)) {
      console.log(day)
      var today = day.getDate();
      var month = day.getMonth() + 1;
      var year = day.getFullYear();
      var nDate = today + '-' + month + '-' + year;
      total = total + this.getTotalValue(rowdata['"' + nDate.toString() + '"'])
    }
    console.log("total-->", total)
    this.myGrid.setcellvalue(args.rowindex, 'total', total);
  }

  onSave() {
    let rows = this.myGrid.getrows();
    this.timesheetHeader.empId = "100";
    this.timesheetHeader.fromDate = this.start;
    this.timesheetHeader.toDate = this.end;
    this.timesheetHeader.totalHours = this.totalHours;
    this.timesheetHeader.status = "Submitted";
    this.timesheetHeader.remarks = "Remarks here";
    this.timesheetHeader.modifyUser = "Thulasi";
    let tDetails = new Array();
    for (var day = new Date(this.start); day <= new Date(this.end); day.setDate(day.getDate() + 1)) {
      var today = day.getDate();
      var month = day.getMonth() + 1;
      var year = day.getFullYear();
      var nDate = today + '-' + month + '-' + year;
      for (let i = 0; i < rows.length; i++) {
        let row = rows[i];
        let hours = this.myGrid.getcellvalue(i, '"' + nDate.toString() + '"')
        if (hours > 0) {
          tDetails.push({ clientName: row.client, projectId: row.projectId, taskType: row.billableType, date: nDate.toString(), hours: hours, comments: row.task, modifyUser: "Thulasi" })
        }
      }
    }
    this.timesheetHeader.timesheetDetails = tDetails;
    console.log(this.timesheetHeader);

    this.timesheetService.saveTimesheet(this.timesheetHeader).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
        console.log("Error in saveTimesheet");
      },
    );
  }
}
