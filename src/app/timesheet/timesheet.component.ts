import { TimesheetService } from './../services/timesheet.service';
import { TimesheetDetails } from './../classes/timesheet-details';
import { TimesheetHeader } from './../classes/timesheet-header';
import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit } from '@angular/core';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid'

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TimesheetComponent implements OnInit, AfterViewInit {

  @ViewChild('myGrid') myGrid: jqxGridComponent;
  start: any;
  end: any;
  showStatus: boolean = true;

  constructor(private timesheetService: TimesheetService) { }

  ngOnInit(): void {
    console.log("coming");

  }
  ngAfterViewInit(): void {
  }


  temp = new Array();
  source: any =
    {
      localdata: this.temp,
      datatype: 'array',
      datafields:
        [
          { name: 'client', value: 'client' },
          { name: 'projectId', type: 'string' },
          { name: 'task', type: 'string' },
          { name: 'billableType', type: 'string' },
          { name: 'total', type: 'number' },
        ]
    };
  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '75%';
    }

    return '95%';
  }
  totalHours = 0;

  getTotalValue(value): number {
    if (!isNaN(parseInt(value)))
      return 0 + parseInt(value);
    else
      return 0;
  }


  startDate(event: any): void {
    let selection = event.args;
    let startingDate = selection.date;
    var today1 = startingDate.getDate();
    var month1 = startingDate.getMonth() + 1;
    var year1 = startingDate.getFullYear();
    this.start = year1 + '/' + month1 + '/' + today1;
    console.log(startingDate);
  }

  endDate(event: any): void {
    let selection = event.args;
    let endingDate = selection.date;
    var today2 = endingDate.getDate();
    var month2 = endingDate.getMonth() + 1;
    var year2 = endingDate.getFullYear();
    this.end = year2 + '/' + month2 + '/' + today2;
    console.log(endingDate);
  }
  columns: any[];
  onGetTimesheet(): void {
    console.log(this.start);
    console.log(this.end);
    if (this.start > this.end) {
      alert("Always End date should be greater than start Date");
      return;
    }
    if (this.start == null || this.end == null) {
      alert("All fields are mandatory");
      return;
    }
    let empId = "100";
    this.timesheetService.getTimesheetDetails(empId, this.start, this.end).subscribe(
      response => {
        console.log(response);
        let data = response;
        if (data.length == 0) {
          console.error("No Timesheet is found");
          return;
        }

        console.log(data.timesheetD.length)

        this.showStatus = false;


        
        console.log(this.temp)
        this.columns =
          [{ text: 'Client Name', columntype: 'dropdownlist', datafield: 'client', width: 150 },
          { text: 'Project Id', datafield: 'projectId', columntype: 'textbox', width: 100 },
          { text: 'Task', datafield: 'task', columntype: 'textbox', width: 410 },
          { text: 'Task Type', columntype: 'dropdownlist', datafield: 'billableType', width: 150 }];

        let count = 1
        for (var day = new Date(this.start); day <= new Date(this.end); day.setDate(day.getDate() + 1)) {
          console.log(day)
          var today = day.getDate();
          var month = day.getMonth() + 1;
          var year = day.getFullYear();
          var nDate = today + '-' + month + '-' + year;
          this.source.datafields.push({ name: '"' + nDate + '"', type: "number" });
          this.columns.push({
            text: "<div style=height:16px;>" + day.toString().split(' ')[0] + "</div>" + nDate.toString(), datafield: '"' + nDate.toString() + '"', width: 120, align: 'center', cellsalign: 'center', columntype: 'numberinput', aggregates: ['sum'],
            aggregatesrenderer: (aggregates: any): string => {
              // console.log("coming", aggregates)
              let renderstring = '';
              for (let obj in aggregates) {
                let name = 'Total'
                let value = aggregates[obj];
                console.log("value--" + value)
                renderstring += '<div style="position: relative; margin: 4px; overflow: hidden;">' + name + ': ' + value + '</div>';
              }
              return renderstring;
            }
          });
          console.log(this.columns)
          console.log(nDate)
          console.log(count)
          count = count + 1

        }

        this.columns.push(
          {
            text: 'Total', datafield: 'total', width: 105, align: 'center', columntype: 'numberinput', cellsalign: 'center',
            aggregates: [{
              '<b>Total</b>': (aggregatedValue: number, currentValue: number, column: any, rowdata: any): number => {
                console.log(rowdata);
                console.log(aggregatedValue);
                console.log(currentValue);
                let total = aggregatedValue + currentValue;
                if (!isNaN(total)) {
                  this.totalHours = total;
                  return total;
                }
                else
                  return aggregatedValue;
              }
            }]
          }
        );

        for (let i = 0; i < data.timesheetD.length; i++) {
          let row = data.timesheetD[i];
          let date = new Date(row.date);
          var today = date.getDate();
          var month = date.getMonth() + 1;
          var year = date.getFullYear();
          var nDate = today + '-' + month + '-' + year;
          let total = 0;
          total = total + row.hours;
          this.temp.push({ client: row.clientName, projectId: row.projectId, billableType: row.taskType, task: row.comments, ['"' + nDate + '"']: row.hours, total: total });

        }


      },
      error => {
        console.log(error);
        console.log("Error in getTimesheetDetails");
      },
    );



  }
  dataAdapter: any = new jqx.dataAdapter(this.source);

}
