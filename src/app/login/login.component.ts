import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AdminDetail } from '../classes/admin-detail';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http'
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private adminDetail = new AdminDetail();

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit() {
    if ((this.adminService.isLoggedIn())) {
      this.router.navigate(['/main']);
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  // create the form object.
  form = new FormGroup({
    uName: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  Login(LoginInformation) {
    this.adminDetail.uName = this.uName.value;
    this.adminDetail.password = this.Password.value;
    this.adminService.login(this.adminDetail).subscribe(
      response => {
        let result = response.body;
        let token = result.token;
        localStorage.setItem("token", token);
        localStorage.setItem("id", result);
        this.router.navigate(['/']);
      },
      error => {
        console.log(error);
        console.log("Error in authentication");
      },

    );
  }

  get uName() {
    return this.form.get('uName');
  }

  get Password() {
    return this.form.get('password');
  }

}
