import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './_layout/sidebar/sidebar.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorComponent } from './error/error.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './_layout/header/header.component';
import { SiteLayoutComponent } from './_layout/site-layout/site-layout.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AuthGuard } from "./guards/auth-guard.service";
import { DetailsComponent } from './details/details.component';
import { jqxCalendarModule }   from 'jqwidgets-ng/jqxcalendar';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
import { jqxDateTimeInputModule } from 'jqwidgets-ng/jqxdatetimeinput';
import { NewTimesheetComponent } from './new-timesheet/new-timesheet.component';
import { TimesheetReportComponent } from './timesheet-report/timesheet-report.component';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    SidebarComponent,
    ConfigurationComponent,
    DashboardComponent,
    ErrorComponent,
    LoginComponent,
    HeaderComponent,
    SiteLayoutComponent,
    DetailsComponent,
    TimesheetComponent,
    NewTimesheetComponent,
    TimesheetReportComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    jqxCalendarModule,
    jqxGridModule,
    jqxDateTimeInputModule,
  //  AngularFontAwesomeModule,
    AppRoutingModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
