export class TimesheetHeader {
    empId : string;
	fromDate : string;
	toDate : string ;
    status : string;
    remarks : string;
    totalHours : any;
    modifyUser : string;
    timesheetDetails : any[];
}