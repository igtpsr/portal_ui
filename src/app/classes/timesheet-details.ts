export class TimesheetDetails {
    clientName : string;
	projectId : string;
	taskType : string ;
    date : string;
    comments : string;
    hours : any;
    modifyUser : string;
}